(use-package helm
  :ensure t
  :config
  (use-package helm-dash :ensure t)

  (setq helm-M-x-fuzzy-match        t   )
  (setq helm-dash-browser-func      'eww)
  (setq helm-locate-fuzzy-match     t   )
  (setq helm-recentf-fuzzy-match    t   )
  (setq helm-buffers-fuzzy-matching t   )

  (helm-mode 1)
  :bind  (
          :map helm-find-files-map
               ("<tab>"         . helm-execute-persistent-action)
               ("<left>"        . helm-find-files-up-one-level  )
               ("<right>"       . helm-execute-persistent-action)
               ("C-<backspace>" . helm-find-files-up-one-level  )
               ))

(provide 'helm-config)
