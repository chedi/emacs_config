(use-package hydra
  :ensure t
  :init
  (use-package helm :ensure t)
  :bind (
         ("<C-f12>" . hydra-flycheck/body               )
         ("<S-f11>" . hydra-projectile/body             )
         ("<C-f11>" . hydra-projectile-other-window/body)
         :map python-mode-map
         ("<s-f12>" . hydra-elpy/body           )
         ("<S-f12>" . hydra-elpy-nav-errors/body)
         :map helm-map
         ("<s-f12>" . hydra-helm/body)))

(require 'helm                   )
(require 'hydra-elpy-config      )
(require 'hydra-helm-config      )
(require 'hydra-flycheck-config  )
(require 'hydra-projectile-config)

(provide 'hydra-config)
