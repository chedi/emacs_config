(defun old_junk ()
  ;; just leave it here for future reference on using org mode click
  ;; custom actions
  (add-hook
   'org-mode-hook
   (lambda ()
     ;; (org-defkey org-mode-map [down-mouse-1] 'custom-org-mouse-down-mouse)
     ))

  (defun custom-org-mouse-down-mouse (event)
    (interactive "e")
    (if (not (and (= 1 (event-click-count event))
                  (youtube-thumbnails-click-handler event)))
        (org-mouse-down-mouse event)))

  (defun youtube-thumbnails-click-handler (event)
    (interactive)
    (let* ((pos (event-start event))
           (img (posn-object pos)))
      (if (and img (eq (nth (- (length img) 2) img) :yt_video_id))
          (progn (async-shell-command
                  (format "mpv --quiet https://www.youtube.com/watch?v=%s" (car (last img))))
                 t)
        nil)))

  (defun org-open-youtube-link-from-overlay ()
    (let ((overlay (ov-at)))
      (when (and overlay
                 (ov-val overlay 'org-video-youtube-thumbnails-overlay))
        (let ((video_id (ov-val overlay 'video_id)))
          (when video_id (play-youtube-video-link video_id)
            t)))))

  )
