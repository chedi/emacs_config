(provide 'org-blog-config)

(defun org-blog-config ()
  (require 'json                       )
  (require 'async                      )
  (require 'ox-publish                 )
  (require 'org-blog-drawer            )
  (require 'org-blog-helpers           )
  (require 'org-blog-media-config      )
  (require 'org-blog-html-template     )
  (require 'org-blog-index-generator   )
  (require 'org-blog-video-thumbnails  )
  (require 'org-blog-generation-scripts)
  (require 'org-blog-youtube-thumbnails)

  (setq org-publish-project-alist
        '(("blog-posts"
           :base-extension       "org"
           :exclude              ".*\.data\.org"
           :base-directory       "~/Documents/Org/Blog"
           :html-link-home       "https://blog.epsilon.technology/"
           :publishing-function  org-html-publish-to-html
           :publishing-directory "/var/www/blog.epsilon.technology"
           :preparation-function org-export-pre-generation
           :completion-function  org-export-post-generation

           :language                        "en"
           :with-toc                        nil
           :with-date                       nil
           :with-tags                       t
           :recursive                       t
           :makeindex                       t
           :with-email                      nil
           :with-author                     nil
           :with-drawers                    t
           :with-creator                    nil
           :headline-levels                 4
           :section-numbers                 nil
           :html-validation-link            nil
           :org-html-link-use-abs-url       t
           :html-head-include-default-style nil)

          ("blog-static"
           :recursive            t
           :base-directory       "~/Documents/Org/Blog/"
           :base-extension       "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf\\|webm\\|ogv\\|woff\\|woff2\\|ttf\\|xml\\|txt"
           :publishing-function  org-publish-attachment
           :publishing-directory "/var/www/blog.epsilon.technology/")

          ("blog" :components ("blog-posts" "blog-static"))))

  (setq org-html-format-drawer-function 'custom-org-export-format-drawer)
  (fset 'original-org-html-template (symbol-function 'org-html-template))

  (define-key org-mode-map (kbd "<f9>"  ) 'org-publish-current-project)
  (define-key org-mode-map (kbd "<M-f9>") 'org-publish-all)
  (define-key org-mode-map (kbd "<s-f9>") (lambda () (interactive) (org-publish-all t)))
  (define-key org-mode-map (kbd "<C-f9>") (lambda () (interactive) (org-publish-current-project t)))

  (add-to-list 'auto-mode-alist '("\\.jinja\\'" . jinja2-mode))
  (add-to-list 'ispell-skip-region-alist '("^#+BEGIN_SRC" . "^#+END_SRC"))

  (custom-org-blog-media-config                        )
  (custom-org-html-template-overwrite                  )
  (custom-org-publish-index-generate-theindex-overwrite)
  )
