(provide 'org-blog-drawer)

(defun custom-org-export-format-drawer (name contents)
  (format
   "<a class='expander' href='#'>%s</a>
    <div class='content collapsable'>%s</div>
    <br/><br/>"
   (downcase (replace-regexp-in-string "_" " " name))
   contents))
