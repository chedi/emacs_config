(provide 'org-blog-helpers)

(require 'dash  )
(require 'cl-lib)

(defun date-exporter (date format)
  (if date (org-timestamp-format date format)
    (format-time-string format)))

(defun date-formater (date format)
  (if date (format-time-string format (date-to-time date))
    (format-time-string format)))

(defun split-org-property (property separator)
  (mapcar 'string-trim
          (org-split-string property separator)))

(defun org-document-get-all-keywords ()
  (org-element-map (org-element-parse-buffer 'element) 'keyword
    (lambda (keyword) (cons (org-element-property :key keyword)
                       (org-element-property :value keyword)))))

(defun org-document-get-keyword (KEYWORD)
  (cdr (assoc KEYWORD (org-document-get-all-keywords))))

(defun org-get-current-project-base-dir ()
  (org-get-project-base-dir (buffer-file-name (buffer-base-buffer))))

(defun org-get-project-plist-from-file (file)
  (let* ((project (org-publish-get-project-from-filename file)))
    (if (not project)
        (error "File %s is not part of any known project"
               (buffer-file-name (buffer-base-buffer))))
    (cdr project)))

(defun org-get-project-base-dir (file)
  (file-name-as-directory
   (plist-get (org-get-project-plist-from-file file) :base-directory)))

(defun org-get-project-export-dir (file)
  (file-name-as-directory
   (plist-get (org-get-project-plist-from-file file) :publishing-directory)))

(defun org-in-project-path (file)
  (concat (org-get-current-project-base-dir) file))

(defun org-at-special (type)
  (save-excursion
    (when (and
           (looking-back (concat "\\[\\[\\(?:" type "\\([^][\n\r]+\\)?\\]\\[\\)?[^]]*\\(\\]\\)?"))
           (goto-char (match-beginning 0))
           (looking-at (concat "\\[\\[" type "\\([^][\n\r]+\\)\\]")))
      (match-string-no-properties 1))))

(defun org-global-props (&optional property buffer)
  (unless property (setq property "PROPERTY"))
  (with-current-buffer (or buffer (current-buffer))
    (org-element-map (org-element-parse-buffer) 'keyword
      (lambda (el) (when (string-match property (org-element-property :key el)) el)))))

(defun org-global-prop-value (key &optional buffer)
  (org-element-property :value (org-global-prop key buffer)))

(defun org-global-prop (key &optional buffer)
  (car (org-global-props key buffer)))

(defun org-global-file-prop (key filename)
  (let* ((file          (expand-file-name filename))
         (buffers_names (mapcar (function buffer-file-name) (buffer-list)))
         (already_open  (-contains? buffers_names file))
         (buffer        (find-file-noselect filename))
         (property      (org-global-props key buffer)))
    (when (not already_open)
      (kill-buffer buffer))
    property))

(defun org-global-file-prop-value (key filename)
  (org-element-property :value (car (org-global-file-prop key filename))))

(defun slugify-string (s)
  (replace-regexp-in-string
   " " "-"
   (downcase (replace-regexp-in-string "[^A-Za-z0-9 ]" "" s))))
