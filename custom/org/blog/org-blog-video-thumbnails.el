(defun org-display-inline-video-thumbnails (&optional include-linked refresh beg end)
  (interactive "P")
  (when (display-graphic-p)
    (unless refresh
      (org-remove-inline-video-thumbnails)
      (when (fboundp 'clear-image-cache) (clear-image-cache)))
    (org-with-wide-buffer
     (goto-char (or beg (point-min)))
     (let ((case-fold-search t)
           (file-extension-re (image-file-name-regexp)))
       (while (re-search-forward "[][]\\[\\(?:video\\)" end t)
         (let ((link (save-match-data (org-element-context))))
           (when (and (equal (org-element-property :type link) "video")
                      (let ((parent (org-element-property :parent link)))
                        (or (not (eq (org-element-type parent) 'link))
                            (not (cdr (org-element-contents parent))))))
             (let* ((video (org-element-property :path link))
                    (file (format "/var/tmp/%s.jpg" (file-name-base video))))
               (when (not (file-exists-p file))
                 (shell-command (format "ffmpegthumbnailer -i %s -o %s -s 0" video file)))
               (when (file-exists-p file)
                 (let ((width (cond
                               ((not (image-type-available-p 'imagemagick)) nil)
                               ((eq org-image-actual-width t) nil)
                               ((listp org-image-actual-width)
                                (or (let ((paragraph (let ((e link))
                                                       (while (and (setq e (org-element-property :parent e))
                                                                   (not (eq (org-element-type e) 'paragraph))))
                                                       e)))
                                      (when paragraph
                                        (save-excursion
                                          (goto-char (org-element-property :begin paragraph))
                                          (when (re-search-forward "^[ \t]*#\\+attr_.*?: +.*?:width +\\(\\S-+\\)"
                                                                   (org-element-property :post-affiliated paragraph) t)
                                            (string-to-number (match-string 1))))))
                                    (car org-image-actual-width)))
                               ((numberp org-image-actual-width)
                                org-image-actual-width)))
                       (old (get-char-property-and-overlay (org-element-property :begin link) 'org-video-video-thumbnails-overlay)))
                   (if (and (car-safe old) refresh)
                       (image-refresh (overlay-get (cdr old) 'display))
                     (let ((image (create-image file (and width 'imagemagick) nil :width width :video video)))
                       (when image
                         (let* ((link (let ((parent (org-element-property :parent link)))
                                        (if (eq (org-element-type parent) 'link) parent link)))
                                (ov (make-overlay (org-element-property :begin link)
                                                  (progn (goto-char (org-element-property :end link))
                                                         (skip-chars-backward " \t") (point)))))
                           (overlay-put ov 'display image)
                           (overlay-put ov 'face 'default)
                           (overlay-put ov 'video video)
                           (overlay-put ov 'org-video-video-thumbnails-overlay t)
                           (overlay-put ov 'modification-hooks (list 'org-display-inline-video-thumbnails-remove-overlay))
                           (push ov org-video-video-thumbnails-overlays)))))))))))))))

(defun org-display-inline-video-thumbnails-remove-overlay (ov after _beg _end &optional _len)
  (let ((inhibit-modification-hooks t))
    (when (and ov after)
      (delete ov org-video-video-thumbnails-overlays)
      (delete-overlay ov))))

(defun org-remove-inline-video-thumbnails ()
  (interactive)
  (mapc #'delete-overlay org-video-video-thumbnails-overlays)
  (setq org-video-video-thumbnails-overlays nil))

(defun org-toggle-inline-video-thumbnails (&optional include-linked)
  (interactive "P")
  (if org-video-video-thumbnails-overlays
      (progn
        (org-remove-inline-video-thumbnails)
        (when (called-interactively-p 'interactive)
          (message "Inline video thumbnails  display turned off")))
    (org-display-inline-video-thumbnails include-linked)
    (when (called-interactively-p 'interactive)
      (message
       (if org-video-video-thumbnails-overlays
           (format "%d images displayed inline"
                   (length org-video-video-thumbnails-overlays))
         "No video thumbnail to display inline")))))

(defun org-redisplay-inline-video-thumbnails ()
  (interactive)
  (if (not org-video-video-thumbnails-overlays)
      (org-toggle-inline-video-thumbnails)
    (org-toggle-inline-video-thumbnails)
    (org-toggle-inline-video-thumbnails)))

(defvar-local org-video-video-thumbnails-overlays nil)

(defcustom org-startup-with-inline-video-thumbnails nil
  "Non-nil means show video videos thumbnails when loading a new Org file.
     This can also be configured on a per-file basis by adding one of
     the following lines anywhere in the buffer:
     #+STARTUP: inlinevideothumbnails
     #+STARTUP: noinlinevideothumbnails"
  :group 'org-startup
  :version "24.1"
  :type 'boolean)


;;(push '("inlinevideothumbnails" org-startup-with-inline-video-thumbnails t) org-startup-options)
;;(push '("noinlinevideothumbnails" org-startup-with-inline-video-thumbnails nil) org-startup-options)

(provide 'org-blog-video-thumbnails)
