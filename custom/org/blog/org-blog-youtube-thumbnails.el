(provide 'org-blog-youtube-thumbnails)

(defun org-display-inline-youtube-thumbnails (&optional include-linked refresh beg end)
  (interactive "P")
  (when (display-graphic-p)
    (unless refresh
      (org-remove-inline-youtube-thumbnails)
      (when (fboundp 'clear-image-cache) (clear-image-cache)))
    (org-with-wide-buffer
     (goto-char (or beg (point-min)))
     (let ((case-fold-search t)
           (file-extension-re (image-file-name-regexp)))
       (while (re-search-forward "[][]\\[\\(?:youtube\\)" end t)
         (let ((link (save-match-data (org-element-context))))
           (when (and (equal (org-element-property :type link) "youtube")
                      (let ((parent (org-element-property :parent link)))
                        (or (not (eq (org-element-type parent) 'link))
                            (not (cdr (org-element-contents parent))))))
             (let* ((video_id (org-element-property :path link))
                    (file (format "/var/tmp/%s.jpg" (org-link-unescape video_id))))
               (when (not (file-exists-p file))
                 (url-copy-file (format "http://img.youtube.com/vi/%s/0.jpg" video_id) file))
               (when (file-exists-p file)
                 (let ((width (cond
                               ((not (image-type-available-p 'imagemagick)) nil)
                               ((eq org-image-actual-width t) nil)
                               ((listp org-image-actual-width)
                                (or (let ((paragraph (let ((e link))
                                                       (while (and (setq e (org-element-property :parent e))
                                                                   (not (eq (org-element-type e) 'paragraph))))
                                                       e)))
                                      (when paragraph
                                        (save-excursion
                                          (goto-char (org-element-property :begin paragraph))
                                          (when (re-search-forward "^[ \t]*#\\+attr_.*?: +.*?:width +\\(\\S-+\\)"
                                                                   (org-element-property :post-affiliated paragraph) t)
                                            (string-to-number (match-string 1))))))
                                    (car org-image-actual-width)))
                               ((numberp org-image-actual-width)
                                org-image-actual-width)))
                       (old (get-char-property-and-overlay (org-element-property :begin link) 'org-video-youtube-thumbnails-overlay)))
                   (if (and (car-safe old) refresh)
                       (image-refresh (overlay-get (cdr old) 'display))
                     (let ((image (create-image file (and width 'imagemagick) nil :width width :yt_video_id video_id)))
                       (when image
                         (let* ((link (let ((parent (org-element-property :parent link)))
                                        (if (eq (org-element-type parent) 'link) parent link)))
                                (ov (make-overlay (org-element-property :begin link)
                                                  (progn (goto-char (org-element-property :end link))
                                                         (skip-chars-backward " \t") (point)))))
                           (overlay-put ov 'display image)
                           (overlay-put ov 'face 'default)
                           (overlay-put ov 'video_id video_id)
                           (overlay-put ov 'org-video-youtube-thumbnails-overlay t)
                           (overlay-put ov 'modification-hooks (list 'org-display-inline-youtube-thumbnails-remove-overlay))
                           (push ov org-video-youtube-thumbnails-overlays)))))))))))))))

(defun org-display-inline-youtube-thumbnails-remove-overlay (ov after _beg _end &optional _len)
  (let ((inhibit-modification-hooks t))
    (when (and ov after)
      (delete ov org-video-youtube-thumbnails-overlays)
      (delete-overlay ov))))

(defun org-remove-inline-youtube-thumbnails ()
  (interactive)
  (mapc #'delete-overlay org-video-youtube-thumbnails-overlays)
  (setq org-video-youtube-thumbnails-overlays nil))

(defun org-toggle-inline-youtube-thumbnails (&optional include-linked)
  (interactive "P")
  (if org-video-youtube-thumbnails-overlays
      (progn
        (org-remove-inline-youtube-thumbnails)
        (when (called-interactively-p 'interactive)
          (message "Inline youtube thumbnails  display turned off")))
    (org-display-inline-youtube-thumbnails include-linked)
    (when (called-interactively-p 'interactive)
      (message
       (if org-video-youtube-thumbnails-overlays
           (format "%d images displayed inline"
                   (length org-video-youtube-thumbnails-overlays))
         "No youtube thumbnail to display inline")))))

(defun org-redisplay-inline-youtube-thumbnails ()
  (interactive)
  (if (not org-video-youtube-thumbnails-overlays)
      (org-toggle-inline-youtube-thumbnails)
    (org-toggle-inline-youtube-thumbnails)
    (org-toggle-inline-youtube-thumbnails)))

(defvar-local org-video-youtube-thumbnails-overlays nil)

(defcustom org-startup-with-inline-youtube-thumbnails nil
  "Non-nil means show youtube videos thumbnails when loading a new Org file.
     This can also be configured on a per-file basis by adding one of
     the following lines anywhere in the buffer:
     #+STARTUP: inlineyoutubethumbnails
     #+STARTUP: noinlineyoutubethumbnails"
  :group 'org-startup
  :version "24.1"
  :type 'boolean)

;; (push '("inlineyoutubethumbnails"   org-startup-with-inline-youtube-thumbnails t  ) org-startup-options)
;; (push '("noinlineyoutubethumbnails" org-startup-with-inline-youtube-thumbnails nil) org-startup-options)
