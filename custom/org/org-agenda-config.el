(defun todo-org-agenda-done (&optional arg)
  (interactive "P")
  (org-agenda-todo "DONE"))

(defun todo-org-agenda-cancel (&optional arg)
  (interactive "P")
  (org-agenda-todo "CANCELLED"))

(defun toggle-tasks-status-visibility (status)
  (let ((status_regex (concat "-^" status)))
    (setq old_regexp_filter
          (if (member status_regex org-agenda-regexp-filter)
              (delete status_regex org-agenda-regexp-filter)
            (push status_regex org-agenda-regexp-filter)))
    (org-agenda-filter-show-all-re)
    (setq org-agenda-regexp-filter old_regexp_filter)
    (org-agenda-filter-apply org-agenda-regexp-filter 'regexp)))

(defun toggle-done-tasks (&optional arg)
  (interactive "P")
  (toggle-tasks-status-visibility "DONE"))

(defun toggle-canceled-tasks (&optional arg)
  (interactive "P")
  (toggle-tasks-status-visibility "CANCELED"))

(defun todo-org-agenda-new ()
  (interactive)
  (org-agenda-switch-to)
  (org-capture 0))

(defun todo-done-followup ()
  (interactive)
  (org-agenda-todo "DONE")
  (org-agenda-switch-to)
  (org-capture 0 "t"))

(defun load-org-agenda-files-recursively (dir)
  (require 'org)
  "Find all directories in DIR."
  (unless (file-directory-p dir) (error "Not a directory `%s'" dir))
  (unless (equal (directory-files dir nil org-agenda-file-regexp t) nil)
    (add-to-list 'org-agenda-files dir))
  (dolist (file (directory-files dir nil nil t))
    (unless (member file '("." ".."))
      (let ((file (concat dir file "/")))
        (when (file-directory-p file)
          (load-org-agenda-files-recursively file))))))

(defun clock-in-to-inptogress (state)
  (if (and (string-equal state "TODO")
           (not (and (boundp 'org-capture-mode) org-capture-mode)))
      (let ((subtree-end (save-excursion (org-end-of-subtree t)))
            (has-subtask nil))
        (save-excursion
          (forward-line 1)
          (while (and (not has-subtask)
                      (< (point) subtree-end)
                      (re-search-forward "^\*+ " subtree-end t))
            (when (member (org-get-todo-state) org-not-done-keywords)
              (setq has-subtask t))))
        (when (not has-subtask)
          "IN PROGRESS"))))

(defun stop-clock-on-exit ()
  (when (org-clock-is-active)
    (org-clock-out nil t)
    (org-save-all-org-buffers)))

(defun redo-all-agenda-buffers ()
  (interactive)
  (dolist (buffer (buffer-list))
    (with-current-buffer buffer
      (when (derived-mode-p 'org-agenda-mode)
        (org-agenda-redo t)))))

(defun show_scrum_agenda ()
  (interactive)
  (let ((agenda_buffer (get-buffer "*Org Agenda(w)*")))
    (if agenda_buffer
        (let ((agenda_window (get-buffer-window agenda_buffer)))
          (if  agenda_window
              (quit-restore-window agenda_window)
            (switch-to-buffer agenda_buffer)))
      (org-agenda nil "w")
      (toggle-done-tasks)
      (toggle-canceled-tasks))))

(use-package org-agenda
  :init
  (add-hook 'kill-emacs-hook                  'stop-clock-on-exit     )
  (add-hook 'org-after-refile-insert-hook     'redo-all-agenda-buffers)
  (add-hook 'org-after-todo-state-change-hook 'redo-all-agenda-buffers)

  (setq org-clock-in-switch-to-state 'clock-in-to-inptogress)

  (setq org-agenda-sticky                       t                           )
  (setq org-clock-persist                       'history                    )
  (setq org-habit-show-habits                   nil                         )
  (setq org-habit-graph-column                  80                          )
  (setq org-habit-preceding-days                7                           )
  (setq org-agenda-block-separator              ?▰                          )
  (setq org-agenda-show-outline-path            nil                         )
  (setq org-clock-clocked-in-display            'mode-line                  )
  (setq org-agenda-start-with-log-mode          nil                         )
  (setq org-agenda-show-inherited-tags          nil                         )
  (setq org-agenda-todo-keyword-format          "%-13s"                     )
  (setq org-agenda-start-with-follow-mode       nil                         )
  (setq org-habit-show-habits-only-for-today    nil                         )
  (setq org-clock-out-remove-zero-time-clocks   t                           )
  (setq org-agenda-restore-windows-after-quit   t                           )
  (setq org-agenda-start-with-clockreport-mode  nil                         )
  (setq org-clock-clocktable-default-properties '(:maxlevel nil :scope file))
  (setq org-agenda-clockreport-parameter-plist  '(:maxlevel nil :link  t   ))

  (setq org-habit-today-glyph     ?⯮)
  (setq org-habit-completed-glyph ?⏻)

  (setq org-gtd-directory   (concat org-directory "Gtd/"  ))
  (setq org-ideas-directory (concat org-directory "Ideas/"))
  (setq org-notes-directory (concat org-directory "Notes/"))

  (load-org-agenda-files-recursively org-gtd-directory  )
  (load-org-agenda-files-recursively org-ideas-directory)
  (load-org-agenda-files-recursively org-notes-directory)

  (setq
   org-capture-templates
   `(("g" "GTD")
     ("gg" "GTD"      entry (file (concat org-gtd-directory "Gtd.org"              )) "* TODO %?\n%U\n" :clock-in t :clock-resume t)
     ("gw" "Projects" entry (file (concat org-gtd-directory "Projects/Projects.org")) "* TODO %?\n%U\n" :clock-in t :clock-resume t)
     ("gp" "Personal" entry (file (concat org-gtd-directory "Personal/Personal.org")) "* TODO %?\n%U\n" :clock-in t :clock-resume t)

     ("n" "Note")
     ("ne" "Emacs"    entry (file (concat org-notes-directory "Emacs.org"   )) "* %? :NOTE:\n%U\n" :clock-in t :clock-resume t)
     ("ne" "Linux"    entry (file (concat org-notes-directory "Linux.org"   )) "* %? :NOTE:\n%U\n" :clock-in t :clock-resume t)
     ("np" "Personal" entry (file (concat org-notes-directory "Personal.org")) "* %? :NOTE:\n%U\n" :clock-in t :clock-resume t)
     ("nw" "Projects" entry (file (concat org-notes-directory "Projects.org")) "* %? :NOTE:\n%U\n" :clock-in t :clock-resume t)

     ("i" "Ideas" entry (file (concat org-ideas-directory "Ideas.org")) "* TODO %? \n%U\n" :clock-in t :clock-resume t)

     ("p" "Protocol"      entry (file+headline ,(concat org-notes-directory "Notes.org") "Navigation") "* %^{Title}\nSource: %:link, %c\n #+BEGIN_QUOTE\n%i\n#+END_QUOTE\n\n\n%? %(progn (setq kk/delete-frame-after-capture 2) \"\")")
     ("L" "Protocol Link" entry (file+headline ,(concat org-notes-directory "Notes.org") "Navigation") "* %? [[%:link][%:description]] \nCaptured On: %U                         %(progn (setq kk/delete-frame-after-capture 2) \"\")")))

  (defvar kk/delete-frame-after-capture 0)

  (defun kk/delete-frame-if-neccessary (&rest r)
    (cond
     ((= kk/delete-frame-after-capture 0) nil)
     ((> kk/delete-frame-after-capture 1)
      (setq kk/delete-frame-after-capture (- kk/delete-frame-after-capture 1)))
     (t
      (setq kk/delete-frame-after-capture 0)
      (delete-frame))))

  (advice-add 'org-capture-kill     :after 'kk/delete-frame-if-neccessary)
  (advice-add 'org-capture-refile   :after 'kk/delete-frame-if-neccessary)
  (advice-add 'org-capture-finalize :after 'kk/delete-frame-if-neccessary)

  (setq
     org-agenda-custom-commands
     '(("w"  "Scrum Boards"
        ((agenda "" ((org-agenda-ndays 7)
                     (org-agenda-start-on-weekday 0)
                     (org-agenda-repeating-timestamp-show-all t)
                     (org-agenda-entry-types '(:timestamp :sexp))
                     (org-agenda-overriding-header (concat "▛▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▜\n"
                                                           "➐ This week      ❱❱\n"
                                                           "▙▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▟\n"))))

         (agenda "" ((org-agenda-span 1)
                     (org-deadline-warning-days 7)
                     (org-agenda-repeating-timestamp-show-all t)
                     (org-agenda-overriding-header (concat "▛▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▜\n"
                                                           "➊ Today          ❱❱\n"
                                                           "▙▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▟\n"))))

         (todo "TODO|IN PROGRESS|BROKEN|BUGGY|DONE|CANCELED"
               ((org-agenda-sorting-strategy '(category-keep))
                (org-agenda-files (list (concat org-gtd-directory "/Personal/")))
                (org-agenda-overriding-header (concat "▛▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▜\n"
                                                      "✪ Personal Todos ❱❱\n"
                                                      "▙▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▟\n"))))


         (todo "TODO|IN PROGRESS|BROKEN|BUGGY|DONE|CANCELED"
               ((org-agenda-sorting-strategy '(category-keep))
                (org-agenda-files (list (concat org-gtd-directory "/Projects/")))
                (org-agenda-overriding-header (concat "▛▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▜\n"
                                                      "⛟ Projects Todos❱❱\n"
                                                      "▙▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▟\n"))))

         (todo "TODO|IN PROGRESS|BROKEN|BUGGY|DONE|CANCELED"
               ((org-agenda-files (list (concat org-gtd-directory "/Gtd.org")))
                (org-agenda-sorting-strategy '(category-keep))
                (org-agenda-overriding-header (concat "▛▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▜\n"
                                                      "⛟ GTDs Todos    ❱❱\n"
                                                      "▙▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▟\n"))))

         (todo "TODO|IN PROGRESS|BROKEN|BUGGY|DONE|CANCELED"
               ((org-agenda-files (list org-ideas-directory))
                (org-agenda-sorting-strategy '(category-keep))
                (org-agenda-overriding-header (concat "▛▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▀▜\n"
                                                      "⌬ Ideas          ❱❱\n"
                                                      "▙▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▄▟\n")))))

        ((org-agenda-prefix-format
          '((agenda   . " %-15c%?-12t%s")
            (timeline . " % s"          )
            (todo     . " %-15c "     )
            (tags     . " %-15c "     )))))))
  :config
  (defun remove-headline-tags (headline)
    (if (string-match "\\(.+\\)[ \t]+\\(:.+:\\)"  headline)
        (string-trim (match-string 1 headline))
      headline))

  (defun org-agenda-highlight-todo (x)
    (let ((org-done-keywords org-done-keywords-for-agenda)
          (case-fold-search nil) (level nil) (position nil) re)
      (if (eq x 'line)
          (save-excursion
            (beginning-of-line 1)
            (setq re (org-get-at-bol 'org-todo-regexp))
            (setq position (or (text-property-any (point-at-bol) (point-at-eol) 'org-heading t) (point)))
            (goto-char position)
            (setq level (get-text-property position 'level))
            (when (looking-at (concat "[ \t]*\\.*\\(" re "\\) +"))
              (add-text-properties (match-beginning 0) (match-end 1)
                                   (list 'face (org-get-todo-face 1)))
              (let* ((s (buffer-substring (match-beginning 1) (match-end 1)))
                     (data (buffer-substring (match-end 1) (line-end-position)))
                     (formated_s (format org-agenda-todo-keyword-format s)))
                (delete-region (match-beginning 1) (point-at-eol))
                (goto-char (match-beginning 1))
                (insert (concat "⎥ " formated_s " ⎥ "
                                (if (string-match "\\(.+\\)\\(\\[#[A-Z]?\\]\\) \\(.+\\)" data)
                                    (concat (string-trim (match-string 2 data))
                                            " ⎥" level (remove-headline-tags (match-string 3 data)))
                                  (concat "     ⎥" level (remove-headline-tags (string-trim data)))))))))
        (let ((pl (text-property-any 0 (length x) 'org-heading t x)))
          (setq re (get-text-property 0 'org-todo-regexp x))
          (when (and re pl (equal (string-match (concat "\\(\\.*\\)" re "\\( +\\)") x pl)
                                  pl))
            (add-text-properties
             (or (match-end 1) (match-end 0)) (match-end 0)
             (list 'face (org-get-todo-face (match-string 2 x)))
             x)
            (when (match-end 1)
              (setq x (concat (substring x 0 (match-end 1)) "⎥ "
                              (format org-agenda-todo-keyword-format (match-string 2 x)) " ⎥ "
                              (let ((data  (substring x (match-end 3))))
                                (if (string-match "\\(.*\\)\\(\\[#[A-Z]?\\]\\) \\(.*\\)" data)
                                    (concat (match-string 2 data) " ⎥"
                                            (or level (get-text-property 0 'level x))
                                            (match-string 1 data)
                                            (remove-headline-tags (match-string 3 data)))
                                  (concat "     ⎥"
                                          (or level (get-text-property 0 'level x))
                                          (remove-headline-tags data))))
                              (org-add-props " " (text-properties-at 0 x))
                              )))))
        x)))

  (key-chord-define-global ":!" 'org-agenda     )
  (key-chord-define-global "xx" 'org-capture    )
  (key-chord-define-global "!:" 'org-agenda-list)

  :bind
  (("<f11>"      . show_scrum_agenda                )
   ("C-²"        . org-agenda                       )
   ("C-&"        . org-todo-list                    )
   ("C-="        . org-agenda-todo                  )
   ("C-c a"      . org-agenda                       )
   ("<s-escape>" . org-capture                      )
   ;; TODO: move these to the org config
   ("C-c l"      . org-insert-link                  )
   ("C-s-e"      . org-babel-execute-and-show-images)
   :map org-agenda-mode-map
   ("M-d" . toggle-done-tasks     )
   ("M-c" . toggle-canceled-tasks )
   ("S"   . org-agenda-schedule   )
   ("C-f" . todo-done-followup    )
   ("C-n" . todo-org-agenda-new   )
   ("C-d" . todo-org-agenda-done  )
   ("C-c" . todo-org-agenda-cancel)))


(provide 'org-agenda-config)
