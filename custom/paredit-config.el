(provide 'paredit-config)

(use-package paredit
  :ensure t

  :bind (
         :map paredit-mode-map
              ("<C-M-right>" . paredit-forward     )
              ("<C-M-left>"  . paredit-backward    )
              ("<C-M-up>"    . paredit-backward-up )
              ("<C-M-down>"  . paredit-forward-down)
              ("<C-M-next>"  . paredit-forward-up  )
              ("<C-M-prior>" . paredit-backward-up )
))
