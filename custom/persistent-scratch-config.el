(use-package persistent-scratch
  :ensure t

  :config
  (persistent-scratch-autosave-mode 1)

  (defun immortal-scratch ()
    (if (eq (current-buffer) (get-buffer "*scratch*"))
        (progn (bury-buffer) nil)
      t))

  (setq persistent-scratch-autosave-interval 60)
  (setq persistent-scratch-autosave-mode t)
  (setq persistent-scratch-backup-directory "~/.emacs.d/scratch")

  (add-hook 'kill-emacs-hook 'persistent-scratch-save)
  (add-hook 'emacs-startup-hook 'persistent-scratch-restore)
  (add-hook 'kill-buffer-query-functions 'immortal-scratch)

  (provide 'persistent-scratch-config)
  )