(use-package pomodoro
  :defer  t
  :ensure t

  :init
  (setq pomodoro-time-format " %.2m:%.2s")

  :config
  (pomodoro-start nil)
  (pomodoro-add-to-mode-line))

(provide 'pomodoro-config)
