(use-package helm-rtags
  :ensure t
  :after  rtags
  :config
  (setq rtags-display-result-backend 'helm))

(use-package rtags
  :ensure t
  :config
  (add-hook 'c-mode-hook   'rtags-start-process-unless-running)
  (add-hook 'c++-mode-hook 'rtags-start-process-unless-running)

  (setq rtags-completions-enabled   t)
  (setq rtags-autostart-diagnostics t)

  :bind (
         :map c-mode-base-map
              ("s-!"   . rtags-location-stack-back   )
              ("s-:"   . rtags-location-stack-forward)
              ("C-."   . rtags-imenu                 )
              ("C-M-;" . rtags-find-symbol-at-point)))

(use-package company-rtags
  :ensure t
  :config
  (eval-after-load 'company
    '(add-to-list 'company-backends 'company-rtags))

  :bind (
         :map c-mode-base-map
              ("<C-tab>" . company-complete)))

(use-package company-irony
  :ensure t
  :config
  (eval-after-load 'company
    '(add-to-list 'company-backends 'company-irony)))

(use-package irony
  :ensure t
  :config
  (irony-cdb-autosetup-compile-options))

(use-package flycheck-irony
  :ensure t
  :config
  (eval-after-load 'flycheck
    '(add-hook 'flycheck-mode-hook #'flycheck-irony-setup)))

(defun cx_mode_customization ()
  (irony-mode                         )
  (semantic-mode                      )
  (global-semantic-mru-bookmark-mode  )
  (rtags-start-process-unless-running )
  (global-semantic-idle-scheduler-mode)

  (define-key c-mode-base-map (kbd "<f6>" ) 'semantic-ia-fast-jump    )
  (define-key c-mode-base-map (kbd "<f7>" ) 'semantic-mrub-switch-tags)
  (define-key c-mode-base-map (kbd "<f8>" ) 'semantic-symref          )
  (define-key c-mode-base-map (kbd "<f9>" ) 'compile                  )
  (define-key c-mode-base-map (kbd "<f10>") 'gdb                      ))

(add-hook 'c-mode-hook   'cx_mode_customization)
(add-hook 'c++-mode-hook 'cx_mode_customization)

(provide 'cpp-config)
