(defun pretty-lambdas-haskell ()
  (font-lock-add-keywords
   nil `((,(concat "\\(" (regexp-quote "\\") "\\)")
          (0 (progn (compose-region (match-beginning 1) (match-end 1)
                                    ,(make-char 'greek-iso8859-7 107))
                    nil))))))

(use-package haskell-mode
  :mode ("\\.hs\\'" . haskell-mode)
  :ensure t
  :init
  (add-hook 'haskell-mode-hook 'haskell-doc-mode        )
  ;; (add-hook 'haskell-mode-hook 'lsp-haskell-enable      )
  (add-hook 'haskell-mode-hook 'pretty-lambdas-haskell  )
  (add-hook 'haskell-mode-hook 'haskell-decl-scan-mode  )
  (add-hook 'haskell-mode-hook 'interactive-haskell-mode)

  :config
  (setq ebal-operation-mode    'stack                          )
  (setq haskell-program-name   "ghci \"+.\""                   )
  (setq haskell-hoogle-command "~/.local/bin/hoogle -i --color")

  (key-chord-define haskell-mode-map "çç" 'haskell-process-reload)
  (key-chord-define haskell-mode-map "!!" 'ebal-execute          )

  (eval-after-load 'flycheck
    '(add-hook 'flycheck-mode-hook #'flycheck-haskell-setup))

  :bind (
         :map haskell-mode-map
              ("s-<"        . insert-left-arrow )
              ("s->"        . insert-right-arrow)
              ("<C-escape>" . helm-hoogle       )))

;; (use-package lsp-mode
;;   :ensure t
;;   :init
;;   (add-hook 'prog-major-mode #'lsp-prog-major-mode-enable))

;; (use-package lsp-haskell
;;   :after  lsp-mode
;;   :ensure t)

(use-package helm-hoogle
  :after  haskell-mode
  :ensure t)

(use-package intero
  :after  haskell-mode
  :ensure t
  :config
  (intero-global-mode 1))

(use-package flycheck-haskell
  :after  haskell-mode
  :ensure t)

(use-package haskell-snippets
  :defer  t
  :ensure t)

(use-package hasky-stack
  :after haskell-mode
  :ensure t

  :bind (
         :map haskell-mode-map
              ("M-s n" . hasky-stack-new    )
              ("M-s e" . hasky-stack-execute)))

(use-package hasky-extensions
  :after haskell-mode
  :ensure t

  :bind (
         :map haskell-mode-map
              ("M-s x" . hasky-extensions)
              ("M-s d" . hasky-extensions-browse-docs)))

(provide 'haskell-config)
