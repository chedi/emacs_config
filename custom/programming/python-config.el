;;; python-config.el --- Python language configuration
;;; Commentary:
;;; Python language configuration
;;; Code:

(use-package cython-mode
  :mode ("\\.pyx\\'" . cython-mode)
  :ensure t
  :config
  (flycheck-mode t))

(use-package python
  :mode        ("\\.py\\'" . python-mode)
  :ensure      t
  :interpreter ("python3" . python-mode)

  :init
  ;; (add-hook 'python-mode-hook 'elpy-mode    )
  (add-hook 'python-mode-hook 'lsp-mode     )
  (add-hook 'python-mode-hook 'smartparens  )
  (add-hook 'python-mode-hook 'company-mode )
  (add-hook 'python-mode-hook 'hs-minor-mode)

  :config
  (key-chord-define python-mode-map ";;" 'sphinx-doc   )
  (key-chord-define python-mode-map "vv" 'pyvenv-workon)

  (use-package pyvenv :ensure t)

  (when (not (getenv "WORKON_HOME"))
    (setenv "WORKON_HOME" "~/.cache/virtualenv"))

  (pyvenv-mode      t      )
  (pyvenv-workon    "emacs")
  (smartparens-mode t      )

  (setq python-check-command "flake8")

  (use-package lsp-mode
    :ensure t
    :commands lsp
    :config

    (require 'lsp-clients)

    (use-package lsp-ui
      :ensure t
      :commands lsp-ui-mode
      :config
      (setq lsp-ui-sideline-ignore-duplicate t)
      (add-hook 'lsp-mode-hook 'lsp-ui-mode))

    (use-package company-lsp
      :ensure t
      :commands company-lsp
      :config
      (push 'company-lsp company-backends))

    (defun lsp-set-cfg ()
        (let ((lsp-cfg `(:pyls (:configurationSources ("flake8")))))
          (lsp--set-configuration lsp-cfg)))

      ;; (add-hook 'python-mode-hook 'lsp)
      (add-hook 'lsp-after-initialize-hook 'lsp-set-cfg))

  (use-package elpy
    :after  python
    :ensure t

    :init
    (add-hook 'elpy-mode-hook 'flycheck-mode)

    :config
    (setq elpy-rpc-backend                "jedi"             )
    (setq elpy-test-django-runner         'elpy-test-runner-p)
    (setq elpy-rpc-large-buffer-size      40960              )
    (setq elpy-interactive-python-command "ipython3"         )

    (setq elpy-modules (quote
                        (elpy-module-eldoc
                         elpy-module-pyvenv
                         elpy-module-django
                         elpy-module-flymake
                         elpy-module-company
                         elpy-module-autodoc
                         elpy-module-yasnippet
                         elpy-module-sane-defaults
                         elpy-module-highlight-indentation )))

    (key-chord-define python-mode-map "!!" 'elpy-find-file                       )
    (key-chord-define python-mode-map "wq" 'elpy-multiedit-python-symbol-at-point)

    (elpy-enable)

    (when (require 'sphinx-doc nil t)
      (sphinx-doc-mode  t ))
    (when (require 'flycheck nil t)
      (setq elpy-modules (delq 'elpy-module-flymake elpy-modules)))

    (defadvice elpy-shell-get-or-create-process (before force-custom-interpreter activate)
      (setf elpy-rpc-python-command         "python3" )
      (setq python-shell-interpreter        "ipython3" python-shell-interpreter-args "-i --simple-prompt")
      (setq elpy-interactive-python-command "ipython3"))

    :bind (
           :map python-mode-map
                ("<C-M-return>" . elpy-goto-definition)
                ("C-s-²"        . elpy-rpc-restart)
                ("<M-RET>"      . elpy-multiedit-python-symbol-at-point))))


(use-package jedi            :ensure t :defer t          )
(use-package flycheck-cython :ensure t :after cython-mode)

(provide 'python-config)

;;; python-config.el ends here