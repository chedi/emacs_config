(use-package scheme
  :mode ("\\.scm\\'" . scheme-mode)
  :ensure t
  :init
  (add-hook 'scheme-mode-hook 'scheme-mode-quack-hook))

(provide 'scheme-config)
