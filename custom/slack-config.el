(use-package slack
  :defer  t
  :ensure t

  :init
  (setq slack-buffer-emojify      t)
  (setq slack-prefer-current-team t)

  :config
  (slack-register-team
   :name                "emacs-slack"
   :default             t
   :token               (secrets-get-secret "emacs" "slack-token"        )
   :client-id           (secrets-get-secret "emacs" "slack-client-id"    )
   :client-secret       (secrets-get-secret "emacs" "slack-client-secret")
   :subscribed-channels '(developpers)))

(provide 'slack-config)
