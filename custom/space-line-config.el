(use-package spaceline
  :ensure t

  :config
  (require 'spaceline-config)

  (spaceline-emacs-theme)
  (spaceline-helm-mode  )

  (spaceline-toggle-minor-modes-off    )
  (spaceline-toggle-flycheck-info-on   )
  (spaceline-toggle-flycheck-error-on  )
  (spaceline-toggle-flycheck-warning-on))

(provide 'space-line-config)
