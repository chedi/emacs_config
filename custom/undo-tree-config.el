(use-package undo-tree
  :ensure t

  :init
  (setq undo-tree-visualizer-diff                nil                                    )
  (setq undo-tree-auto-save-history              nil                                    )
  (setq undo-tree-visualizer-timestamps          t                                      )
  (setq undo-tree-history-directory-alist        '(("." . "~/.emacs.d/undo_tree_saves")))
  (setq undo-tree-visualizer-relative-timestamps nil                                    ))

(provide 'undo-tree-config)
