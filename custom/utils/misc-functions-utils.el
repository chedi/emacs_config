(provide 'misc-functions-utils)

(defalias 'yes-or-no-p 'y-or-n-p)

(defmacro hook-into-modes (func modes) `(dolist (mode-hook ,modes) (add-hook mode-hook ,func)))

(defun insert-tab ()
  "Insert a tab char"
  (interactive)
  (insert-char 9))

(defun insert-right-arrow ()
  "Insert ->"
  (interactive)
  (insert-string "->"))

(defun insert-left-arrow ()
  "Insert <-"
  (interactive)
  (insert-string "<-"))

(defun insert-current-timestamp ()
  "Spit out the current time in Y-m-d format."
  (interactive)
  (insert (format-time-string "%Y-%m-%d")))

(defun start-bash ()
  "start a bash terminal"
  (interactive)
  (ansi-term "/bin/bash"))

(defun kill-current-buffer ()
  "kill current buffer"
  (interactive)
  (kill-buffer (current-buffer)))
