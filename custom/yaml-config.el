(use-package yaml-mode
  :ensure t

  :config
  (add-to-list 'auto-mode-alist '("\\.yml\\'" . yaml-mode))

  (add-hook 'yaml-mode-hook
            '(lambda ()
               (electric-pair-mode)
               (define-key yaml-mode-map "\C-m" 'newline-and-indent))))

(provide 'yaml-config)
